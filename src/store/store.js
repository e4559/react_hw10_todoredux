import { combineReducers, createStore } from "redux";
import { tasksReducer } from "./reducers/tasksReducer";
import { statusReducer } from "./reducers/statusReducer";
import { searchReducer } from "./reducers/searchReducer";

const initialState = {
  tasks: [],
  status: {
    important: false,
    done: false,
  },
  searchText: "",
};

export const store = createStore(
  combineReducers({
    tasks: tasksReducer,
    status: statusReducer,
    searchText: searchReducer,
  }),
  initialState
);
