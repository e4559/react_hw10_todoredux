import { ACTION_TYPES } from "../reducers/tasksReducer";
import { ACTION_TYPES_STATUS } from "../reducers/statusReducer";
import { ACTION_TYPES_SEARCH } from "../reducers/searchReducer";

const addTask = (inputValue: string) => {
  return { type: ACTION_TYPES.ADD_NEW_TASK, taskName: inputValue };
};
const deleteTask = (id: number) => {
  return { type: ACTION_TYPES.DELETE_TASK, id: id };
};

const handleEdit = (id: number, newTask: string) => {
  return { type: ACTION_TYPES.HANDLE_EDIT, id: id, newTask };
};

const handleImp = (id: number) => {
  return { type: ACTION_TYPES.HANDLE_IMPORTANT, id: id };
};

const handleDone = (id: number) => {
  return { type: ACTION_TYPES.HANDLE_DONE, id: id };
};

const filterStatus = (name: string) => {
  return {
    type: ACTION_TYPES_STATUS.FILTER_TASKS,
    taskName: name.toLowerCase(),
  };
};
const setSearchText = (text: string) => {
  return { type: ACTION_TYPES_SEARCH.SET_SEARCH_TEXT, text };
};


export {
  addTask,
  filterStatus,
  setSearchText,
  handleEdit,
  handleImp,
  handleDone,
  deleteTask,
}