type actionType = {
  type: string;
  text: string;
};
export const ACTION_TYPES_SEARCH = {
  SET_SEARCH_TEXT: "SET_SEARCH_TEXT",
};

const searchReducer = (state = "", action: actionType) => {
  switch (action.type) {
    case ACTION_TYPES_SEARCH.SET_SEARCH_TEXT: {
      return action.text;
    }
    default:
      return state;
  }
};

export { searchReducer };
