export const ACTION_TYPES = {
  ADD_NEW_TASK: "ADD_NEW_TASK",
  DELETE_TASK: "DELETE_TASK",
  HANDLE_EDIT: "HANDLE_EDIT",
  HANDLE_DONE: "HANDLE_DONE",
  HANDLE_IMPORTANT: "HANDLE_IMPORTANT",
};

type actionType = {
  type: string;
  taskName: string;
  id: number;
  newName: string;
};

// type stateType = {
//   name: string;
//   important: boolean;
//   done: boolean;
//   delete: boolean;
//   id: number;
// }[];

const tasksReducer = (state = [], action: actionType) => {
  switch (action.type) {
    case ACTION_TYPES.ADD_NEW_TASK: {
      if (action.taskName) {
        return [
          ...state,
          {
            name: action.taskName,
            important: false,
            done: false,
            delete: false,
            id: state.length + 1,
          },
        ];
      }
      break;
    }

    case ACTION_TYPES.DELETE_TASK: {
      return state.filter((task: { id: number }) => task.id !== action.id);
    }
    // break;
    case ACTION_TYPES.HANDLE_EDIT: {
      const newState = state.map((task: { id: number; name: string }) => {
        task.id === action.id && (task.name = action.newName);
        return task;
      });
      return newState;
    }
    // break;
    case ACTION_TYPES.HANDLE_IMPORTANT: {
      const newState = state.map((task: { important: boolean; id: number }) => {
        task.id === action.id && (task.important = !task.important);
        return task;
      });
      return newState;
    }
    // break;
    case ACTION_TYPES.HANDLE_DONE: {
      const newState = state.map((task: { done: boolean; id: number }) => {
        task.id === action.id && (task.done = !task.done);
        return task;
      });
      return newState;
    }
    // break;
    default:
      return state;
  }
};

// const selectTasks = (state) => state.tasks;
// const addName = (newName) => {
//   return { type: "add", name: newName };
// };
export { tasksReducer };
/*


  const handleEdit = (id: number, newName: string) => {
    const newState = state.tasks.map((task) => {
      task.id === id && (task.name = newName);
      return task;
    });
    setState({ ...state, tasks: newState });
  };
  const handleImportant = (id: number) => {
    const newState = state.tasks.map((task) => {
      task.id === id && (task.important = !task.important);
      return task;
    });
    setState({ ...state, tasks: newState });
  };

  const handleDone = (id: number) => {
    const newState = state.tasks.map((task) => {
      task.id === id && (task.done = !task.done);
      return task;
    });
    setState({ ...state, tasks: newState });
  };
  const handleDelete = (id: number) => {
    const newState = state.tasks.filter((task) => task.id !== id);
    setState({ ...state, tasks: newState });
  }; */
