// type stateType = {
//   important: boolean;
//   done: boolean;
// };
export const statusVersion = {
  important: "important",
  done: "done",
  all: "all",
};
type actionType = {
  type: string;
  status: "important" | "all" | "done";
};
export const ACTION_TYPES_STATUS = {
  FILTER_TASKS: "FILTER_TASKS",
};

const statusReducer = (state = {}, action: actionType) => {
  switch (action.type) {
    case ACTION_TYPES_STATUS.FILTER_TASKS: {
      if (action.status === statusVersion.important) {
        return { important: true, done: false };
      } else if (action.type === statusVersion.done) {
        return { important: false, done: true };
      } else if (action.type === statusVersion.all) {
        return { important: false, done: false };
      }
      break;
    }

    default:
      return state;
  }
};

export { statusReducer };
