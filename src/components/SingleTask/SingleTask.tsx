import React, { useState, useRef } from "react";
import ButtonSingleTask from "../ui/ButtonSingleTask";
import InputSingleTask from "../ui/InputSingleTask";
import Box from "@mui/material/Box";
import { useStyles } from "./useStyles";
import { useDispatch } from "react-redux";
import {
  handleDone,
  handleEdit,
  handleImp,
  deleteTask,
} from "./../../store/actions/actions";

type SingleTaskProps = {
  task: {
    name: string;
    id: number;
    important: boolean;
    done: boolean;
  };
};

const SingleTask: React.FC<SingleTaskProps> = ({ task }) => {
  const styles = useStyles();
  const [edited, setEdited] = useState(true);
  const [newTask, setTask] = useState("");
  const inputRef = useRef<HTMLInputElement | null>(null);

  const dispatch = useDispatch();

  const handleEditComp = () => {
    inputRef.current?.focus();
    setEdited(!edited);
    dispatch(handleEdit(task.id, newTask));
  };

  const handleImpComp = () => dispatch(handleImp(task.id));
  const handleDoneComp = () => dispatch(handleDone(task.id));
  const handelDelComp = () => dispatch(deleteTask(task.id));

  return (
    <Box className={styles.wrapper}>
      <InputSingleTask
        task={task}
        ref1={inputRef}
        edited={edited}
        onChange={setTask}
      />
      <ButtonSingleTask
        name={edited ? "Edit" : "Save"}
        cn="edit"
        onClick={handleEditComp}
      />
      <ButtonSingleTask
        name="Important"
        cn="important"
        onClick={handleImpComp}
      />
      <ButtonSingleTask name="Done" cn="done" onClick={handleDoneComp} />
      <ButtonSingleTask name="Delete" cn="delete" onClick={handelDelComp} />
    </Box>
  );
};

export default SingleTask;
