import React from "react";
import Heading from "../ui/Heading";
import Input from "../ui/Input";
import Box from "@mui/material/Box";
import ButtonComp from "./../ui/ButtonComp";
import { useStyles } from "./useStyles";
import { useDispatch } from "react-redux";
import { filterStatus, setSearchText } from "../../store/actions/actions";

const SearchTask = () => {
  const styles = useStyles();
  const dispatch = useDispatch();

  const handleClick = (name: string) => {
    dispatch(filterStatus(name));
  };
  return (
    <div>
      <Heading title="Search" />
      <Box className={styles.wrapper}>
        <Input onChange={(e) => dispatch(setSearchText(e))} />

        <ButtonComp name="All" onClick={handleClick} />
        <ButtonComp name="Important" onClick={handleClick} />
        <ButtonComp name="Done" onClick={handleClick} />
      </Box>
    </div>
  );
};

export default SearchTask;
