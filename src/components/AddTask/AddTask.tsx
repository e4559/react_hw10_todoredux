import React, { useState } from "react";
import { useStyles } from "./useStyles";
import Input from "../ui/Input";
import Heading from "./../ui/Heading";
import ButtonAddTask from "../ui/ButtonAddTask";
import Box from "@mui/material/Box";
import { useDispatch } from "react-redux";
// import { ACTION_TYPES } from "../../store/reducers/tasksReducer";
import { addTask } from "../../store/actions/actions";

const AddTask = () => {
  const [inputValue, setInputValue] = useState("");
  const styles = useStyles();
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(addTask(inputValue));
    setInputValue("");
  };
  const handleInput = (e: string) => {
    setInputValue(e);
  };
  return (
    <Box className={styles.wrapper}>
      <Heading title="My Tasks" />
      <Box className={styles.box}>
        <Input onChange={handleInput} value={inputValue} />
        <ButtonAddTask onClick={handleClick} />
      </Box>
    </Box>
  );
};

export default AddTask;
