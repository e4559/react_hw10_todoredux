import React from "react";
import { useSelector } from "react-redux";
import SingleTask from "../SingleTask";
import Heading from "../ui/Heading";

type stateType = {
  tasks: {
    name: string;
    important: boolean;
    done: boolean;
    delete: boolean;
    id: number;
  }[];
  status: {
    important: boolean;
    done: boolean;
  };
  searchText: string;
};

const Tasks = () => {
  const state = useSelector((state: stateType) => state);

  return (
    <div>
      <Heading title="Tasks" />

      {state.tasks
        .filter((task) => {
          if (state.searchText === "") {
            return task;
          } else if (task && task.name.includes(state.searchText)) {
            return task;
          } else {
            return "";
          }
        })
        .map((task) => {
          if (!state.status.important && !state.status.done) {
            return <SingleTask key={task.id} task={task} />;
          } else if (state.status.important && task.important) {
            return <SingleTask key={task.id} task={task} />;
          } else if (state.status.done && task.done) {
            return <SingleTask key={task.id} task={task} />;
          } else {
            return "";
          }
        })}
    </div>
  );
};

export default Tasks;
