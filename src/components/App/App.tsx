import React from "react";
import AddTask from "./../AddTask";
import Box from "@mui/material/Box";
import SearchTask from "../SearchTask";
import Tasks from "../Tasks";
import { useStyles } from "./useStyles";

function App() {
  const styles = useStyles();

  return (
    <Box className={styles.wrapper}>
      <AddTask />
      <SearchTask />
      <Tasks />
    </Box>
  );
}

export default App;
